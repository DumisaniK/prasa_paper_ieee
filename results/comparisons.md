


SVM
===========================================

INIRIA absolute results
---------------------------------
[96.720, 98.040,  97.400 ]
normal = 96.720
processed average = 97.72


NICTA absolute results
--------------------------------
[ 97.740, 97.180, 97.520]
normal =  97.740
processed average = 97.35

SIGNI absolute results
------------------------------------
[98.080, 98.190, 97.970]
normal = 98.080
processed average = 98.08



ELM
===========================================

INIRIA absolute results
---------------------------------
[93.7 , 91.6  , 94.6 ]

normal = 93.7
processed average = 93.1


NICTA absolute results
--------------------------------
[93.7 , 93.2 , 92.5   ]
normal = 93.7
processed average = 92.85

SIGNI absolute results
------------------------------------
[96.1 , 96.4 , 96.6 ]
normal = 96.1
processed average = 96.5





CNN
===========================================
INIRIA absolute results
---------------------------------
[99.32, 98.87,  99.55 ]
normal = 99.32
processed average = 99.21


NICTA absolute results
--------------------------------
[ 99.15, 99.2, 99.25]
normal = 99.15
processed average = 99.225

SIGNI absolute results
------------------------------------
[99.79 , 99.79, 99.48  ]
normal = 99.79
processed average = 99.635



